package com.ing.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ing.dto.entity.Borrower;
import com.ing.repository.BorrowerRepository;
import com.ing.request.BorrowerDto;
import com.ing.request.BorrowerRequest;
import com.ing.request.BorrowerResponse;

@Service
public class BorrowerService {

	@Autowired
	BorrowerRepository borrowerRepo;
	
	public BorrowerResponse persistBroker(BorrowerRequest req){
		borrowerRepo.save(beanMapper(req.getObj()));
		return beanMapper(borrowerRepo.findAll());
	}
	
	public BorrowerResponse getBorrowers(Long id) {
		List<Borrower> bList=borrowerRepo.findById(id);
		return beanMapper(bList);
	}

	
	public BorrowerResponse getBorrowers() {
		return beanMapper(borrowerRepo.findAll());
	}
	
	private Borrower beanMapper(BorrowerDto dto){
		Borrower b=new Borrower();
		b.setAmount(dto.getAmount());
		b.setDetails(dto.getDetails());
		b.setFrequency(dto.getFrequency());
		b.setLoanType(dto.getLoanType());
		b.setMonthlyExpense(dto.getMonthlyExpense());
		b.setMonthlyIncome(dto.getMonthlyIncome());
		b.setPurchaseAddress(dto.getPurchaseAddress());
		return b;
	}

	private BorrowerResponse beanMapper(List<Borrower> bList){
		BorrowerResponse resp=new BorrowerResponse();
		for (Borrower dto : bList) {
			BorrowerDto b=new BorrowerDto();
			b.setId(dto.getId());
			b.setAmount(dto.getAmount());
			b.setDetails(dto.getDetails());
			b.setFrequency(dto.getFrequency());
			b.setLoanType(dto.getLoanType());
			b.setMonthlyExpense(dto.getMonthlyExpense());
			b.setMonthlyIncome(dto.getMonthlyIncome());
			b.setPurchaseAddress(dto.getPurchaseAddress());
			resp.getObj().add(b);
		}
		
		return resp;
	}
	
	private BorrowerResponse beanMapper(Iterable<Borrower> bList){
		BorrowerResponse resp=new BorrowerResponse();
		for (Borrower dto : bList) {
			BorrowerDto b=new BorrowerDto();
			b.setId(dto.getId());
			b.setAmount(dto.getAmount());
			b.setDetails(dto.getDetails());
			b.setFrequency(dto.getFrequency());
			b.setLoanType(dto.getLoanType());
			b.setMonthlyExpense(dto.getMonthlyExpense());
			b.setMonthlyIncome(dto.getMonthlyIncome());
			b.setPurchaseAddress(dto.getPurchaseAddress());
			resp.getObj().add(b);
		}
		
		return resp;
	}

}
