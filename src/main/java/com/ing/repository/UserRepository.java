package com.ing.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ing.dto.entity.User;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

    List<User> findByUserId(Long id);
}

