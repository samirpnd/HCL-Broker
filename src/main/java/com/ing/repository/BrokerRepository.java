package com.ing.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ing.dto.entity.Broker;

@Repository
public interface BrokerRepository extends CrudRepository<Broker, Long> {

    
}

