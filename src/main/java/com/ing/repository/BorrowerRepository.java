package com.ing.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ing.dto.entity.Borrower;

@Repository
public interface BorrowerRepository extends CrudRepository<Borrower, Long> {
 
	List<Borrower> findById(Long id);
	
}
