package com.ing.request;

import java.util.ArrayList;
import java.util.List;

public class BorrowerResponse {

	private List<BorrowerDto> obj=new ArrayList<>();

	public List<BorrowerDto> getObj() {
		return obj;
	}

	public void setObj(List<BorrowerDto> obj) {
		this.obj = obj;
	}
}
