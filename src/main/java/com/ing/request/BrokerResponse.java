package com.ing.request;

import java.util.ArrayList;
import java.util.List;

public class BrokerResponse {

	List<BrokerTo> brokers=new ArrayList<>();

	public List<BrokerTo> getBrokers() {
		return brokers;
	}

	public void setBrokers(List<BrokerTo> brokers) {
		this.brokers = brokers;
	}

	

	}
