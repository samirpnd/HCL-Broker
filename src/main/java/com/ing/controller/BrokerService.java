package com.ing.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ing.dto.entity.Broker;
import com.ing.repository.BrokerRepository;
import com.ing.request.BrokerRequest;
import com.ing.request.BrokerResponse;
import com.ing.request.BrokerTo;

@Service
public class BrokerService {

	@Autowired
	BrokerRepository repo;

	public BrokerResponse updateBroker(BrokerRequest req) {
		Broker b1 = repo.save(beanMapper(req.getBroker()));
		return beanMapper(b1);
	}

	public BrokerResponse getAllBrokers() {
		return beanMapper(repo.findAll());
	}

	private BrokerResponse beanMapper(Iterable<Broker> bList) {
		BrokerResponse resp = new BrokerResponse();
		for (Broker dto : bList) {
			BrokerTo b = new BrokerTo();
			b.setAddress(dto.getAddress());
			b.setDob(dto.getDob());
			b.setPhone(dto.getPhone());
			b.setPassword(dto.getPassword());
			resp.getBrokers().add(b);
		}
		return resp;
	}

	private Broker beanMapper(BrokerTo to) {
		Broker b = null;
		if (to != null) {
			b = new Broker();
			b.setAddress(to.getAddress());
			b.setDob(to.getDob());
			b.setPassword(to.getPassword());
			b.setPhone(to.getPhone());
		}
		return b;
	}

	private BrokerResponse beanMapper(Broker to) {
		BrokerTo b = null;
		BrokerResponse res = new BrokerResponse();
		if (to != null) {
			b = new BrokerTo();
			b.setAddress(to.getAddress());
			b.setDob(to.getDob());
			b.setPhone(to.getPhone());
			res.getBrokers().add(b);
		}
		return res;
	}
}
