package com.ing.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ing.request.BorrowerRequest;
import com.ing.request.BorrowerResponse;
import com.ing.request.BrokerRequest;
import com.ing.request.BrokerResponse;
import com.ing.service.BorrowerService;

@RestController
public class BorrowerController {

	@Autowired
	BorrowerService borrowerService;


	@Autowired
	BrokerService brokerService;

	@RequestMapping(value= "/loan", method = RequestMethod.POST)
	public BorrowerResponse createLoan(@RequestBody BorrowerRequest broker) {
		return borrowerService.persistBroker(broker);
	}
	@RequestMapping(value= "/loans", method = RequestMethod.GET)
	public BorrowerResponse getPipelines() {
			return borrowerService.getBorrowers();
	}
	
	@RequestMapping(value= "/broker", method = RequestMethod.PUT)
	public BrokerResponse updateBroker(@RequestBody BrokerRequest req) {
			return brokerService.updateBroker(req);
	}
	
	@RequestMapping(value= "/brokers", method = RequestMethod.GET)
	public BrokerResponse getBrokers() {
			return brokerService.getAllBrokers();
	}
	
	
}
